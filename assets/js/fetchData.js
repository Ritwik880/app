const userList = document.getElementById('user-list');
const loadMoreButton = document.getElementById('load-more');

let currentPage = 1;
const itemsPerPage = 50;

// function to fetch user data from the JSONPlaceholder API
async function fetchUserData(page) {
    try {
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=${itemsPerPage}`);
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        const data = await response.json();
        renderUserData(data)
    } catch (error) {
        console.error('Error fetching user data:', error);
    }
}

// function to render user data
function renderUserData(users) {
    users.map((item, id) => {
        const div = document.createElement('p');
        const body = document.createElement('h6');
        body.innerHTML= `Name: ${item.title}`
        div.textContent = `Desc: ${item.body}`;
        userList.appendChild(body);
        userList.appendChild(div);
    })
}

// function to load more data and generate random data
async function loadMoreData() {
    currentPage++;
    const userData = await fetchUserData(currentPage);

    if (userData && userData.length > 0) {
        renderUserData(userData);
    } else {
        // if there are no more results, generate random data
        for (let i = 0; i < itemsPerPage; i++) {
            const listItem = document.createElement('div');
            listItem.textContent = `Random User ${i + 1}`;
            userList.appendChild(listItem);
        }
        loadMoreButton.disabled = true;
    }
}

loadMoreButton.addEventListener('click', loadMoreData);

// Initial data load
loadMoreData();
